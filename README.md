# mono-docker-container

---

## Overview

Mono-back is a backend support for mono app

## Instructions

Supported PHP versions: 7.x and 5.6.x.

## Usage

* `make all` - Install all.
* `make info` - Show project services IP addresses.
* `make si` - reinstall site.
* `make clean` - totally remove project build folder, docker containers and network.
* `make reinstall` - rebuild & reinstall site.
* `make exec` - docker exec into php container.
* `make exec0` - docker exec into php container as root.
* `make dev` - Devel + kint setup, and config for Twig debug mode, disable aggregation.
* `make drush [command]` - execute drush command. 
* `make phpcs` - check codebase with `phpcs` sniffers to make sure it conforms https://www.drupal.org/docs/develop/standards.
* `make phpcbf` - fix codebase according to Drupal standards https://www.drupal.org/docs/develop/standards.
* `make cex` - executes config export to `config/sync` directory.
* `make cim` - executes config import from `config/sync` directory.
