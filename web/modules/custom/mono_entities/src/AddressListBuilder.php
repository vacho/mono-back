<?php

namespace Drupal\mono_entities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the address entity type.
 */
class AddressListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * Constructs a new AddressListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter, RedirectDestinationInterface $redirect_destination) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->redirectDestination = $redirect_destination;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('redirect.destination')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['table'] = parent::render();

    $total = \Drupal::database()
      ->query('SELECT COUNT(*) FROM {m_address}')
      ->fetchField();

    $build['summary']['#markup'] = $this->t('Total addresses: @total', ['@total' => $total]);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['full_name'] = $this->t('Full name');
    $header['company'] = $this->t('Company');
    $header['address_line1'] = $this->t('Address line1');
    $header['address_line2'] = $this->t('Address line2');
    $header['address_line3'] = $this->t('Address line3');
    $header['postal_code'] = $this->t('Postal code');
    $header['sorting_code'] = $this->t('Sorting code');
    $header['dependent_locality'] = $this->t('Dependent locality');
    $header['locality'] = $this->t('Locality');
    $header['administrative_area'] = $this->t('Administrative area');
    $header['gps'] = $this->t('Gps');
    $header['uid'] = $this->t('Author');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\mono_entities\AddressInterface */
    $row['id'] = $entity->id();
    $row['full_name'] = $entity->toLink();
    $row['company'] = $entity->getCompany();
    $row['address_line1'] = $entity->getAddressLine1();
    $row['address_line2'] = $entity->getAddressLine2();
    $row['address_line3'] = $entity->getAddressLine3();
    $row['postal_code'] = $entity->getPostalCode();
    $row['sorting_code'] = $entity->getSortingCode();
    $row['dependent_locality'] = $entity->getDependentLocality();
    $row['locality'] = $entity->getLocality();
    $row['administrative_area'] = $entity->getAdministrativeArea();
    $row['gps'] = $entity->getGps();
    $row['uid']['data'] = [
      '#theme' => 'username',
      '#account' => $entity->getOwner(),
    ];
    $row['created'] = $this->dateFormatter->format($entity->getCreatedTime());
    $row['changed'] = $this->dateFormatter->format($entity->getChangedTime());
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $destination = $this->redirectDestination->getAsArray();
    foreach ($operations as $key => $operation) {
      $operations[$key]['query'] = $destination;
    }
    return $operations;
  }

}
