<?php

namespace Drupal\mono_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a currency entity type.
 */
interface CurrencyInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the currency name.
   *
   * @return string
   *   Name of the currency.
   */
  public function getName();

  /**
   * Sets the currency name.
   *
   * @param string $name
   *   The currency name.
   *
   * @return \Drupal\mono_entities\CurrencyInterface
   *   The called currency entity.
   */
  public function setName($name);

  /**
   * Gets the currency creation timestamp.
   *
   * @return int
   *   Creation timestamp of the currency.
   */
  public function getCreatedTime();

  /**
   * Sets the currency creation timestamp.
   *
   * @param int $timestamp
   *   The currency creation timestamp.
   *
   * @return \Drupal\mono_entities\CurrencyInterface
   *   The called currency entity.
   */
  public function setCreatedTime($timestamp);

}
