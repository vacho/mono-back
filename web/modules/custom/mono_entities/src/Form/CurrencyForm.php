<?php

namespace Drupal\mono_entities\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the currency entity edit forms.
 */
class CurrencyForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New currency %label has been created.', $message_arguments));
      $this->logger('mono_entities')->notice('Created new currency %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The currency %label has been updated.', $message_arguments));
      $this->logger('mono_entities')->notice('Created new currency %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.currency.collection', ['currency' => $entity->id()]);
  }

}
