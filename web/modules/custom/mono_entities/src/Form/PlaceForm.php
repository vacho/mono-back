<?php

namespace Drupal\mono_entities\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the place entity edit forms.
 */
class PlaceForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New place %label has been created.', $message_arguments));
      $this->logger('mono_entities')->notice('Created new place %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The place %label has been updated.', $message_arguments));
      $this->logger('mono_entities')->notice('Created new place %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.place.canonical', ['place' => $entity->id()]);
  }

}
