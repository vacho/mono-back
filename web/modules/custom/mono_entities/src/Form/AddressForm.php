<?php

namespace Drupal\mono_entities\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the address entity edit forms.
 */
class AddressForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New address %label has been created.', $message_arguments));
      $this->logger('mono2')->notice('Created new address %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The address %label has been updated.', $message_arguments));
      $this->logger('mono2')->notice('Created new address %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.address.canonical', ['address' => $entity->id()]);
  }

}
