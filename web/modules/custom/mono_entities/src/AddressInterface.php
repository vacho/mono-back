<?php

namespace Drupal\mono_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining an address entity type.
 */
interface AddressInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the address full name.
   *
   * @return string
   *   Title of the address.
   */
  public function getFullName();

  /**
   * Sets the address full name.
   *
   * @param string $full_name
   *   The address full_name.
   *
   * @return \Drupal\mono_entities\AddressInterface
   *   The called address entity.
   */
  public function setFullName($full_name);

  /**
   * Gets the address creation timestamp.
   *
   * @return int
   *   Creation timestamp of the address.
   */
  public function getCreatedTime();

  /**
   * Sets the address creation timestamp.
   *
   * @param int $timestamp
   *   The address creation timestamp.
   *
   * @return \Drupal\mono_entities\AddressInterface
   *   The called address entity.
   */
  public function setCreatedTime($timestamp);

}
