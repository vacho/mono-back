<?php

namespace Drupal\mono_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining an exchange entity type.
 */
interface ExchangeInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the exchange creation timestamp.
   *
   * @return int
   *   Creation timestamp of the exchange.
   */
  public function getCreatedTime();

  /**
   * Sets the exchange creation timestamp.
   *
   * @param int $timestamp
   *   The exchange creation timestamp.
   *
   * @return \Drupal\mono_entities\ExchangeInterface
   *   The called exchange entity.
   */
  public function setCreatedTime($timestamp);

}
