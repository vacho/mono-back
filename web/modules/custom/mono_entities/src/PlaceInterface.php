<?php

namespace Drupal\mono_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a place entity type.
 */
interface PlaceInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the place name.
   *
   * @return string
   *   Name of the place.
   */
  public function getName();

  /**
   * Sets the place name.
   *
   * @param string $name
   *   The place name.
   *
   * @return \Drupal\mono_entities\PlaceInterface
   *   The called place entity.
   */
  public function setName($name);

  /**
   * Gets the place creation timestamp.
   *
   * @return int
   *   Creation timestamp of the place.
   */
  public function getCreatedTime();

  /**
   * Sets the place creation timestamp.
   *
   * @param int $timestamp
   *   The place creation timestamp.
   *
   * @return \Drupal\mono_entities\PlaceInterface
   *   The called place entity.
   */
  public function setCreatedTime($timestamp);

}
