<?php

namespace Drupal\mono_entities\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\mono_entities\RecurrentInterface;
use Drupal\user\UserInterface;

/**
 * Defines the recurrent entity class.
 *
 * @ContentEntityType(
 *   id = "recurrent",
 *   label = @Translation("Recurrent"),
 *   label_collection = @Translation("Recurrents"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\mono_entities\RecurrentListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\mono_entities\RecurrentAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\mono_entities\Form\RecurrentForm",
 *       "edit" = "Drupal\mono_entities\Form\RecurrentForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "m_recurrent",
 *   data_table = "m_recurrent_field_data",
 *   revision_table = "m_recurrent_revision",
 *   revision_data_table = "m_recurrent_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer recurrent",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/recurrent/add",
 *     "canonical" = "/recurrent/{recurrent}",
 *     "edit-form" = "/admin/content/recurrent/{recurrent}/edit",
 *     "delete-form" = "/admin/content/recurrent/{recurrent}/delete",
 *     "collection" = "/admin/content/recurrent"
 *   },
 *   field_ui_base_route = "entity.recurrent.settings"
 * )
 */
class Recurrent extends RevisionableContentEntityBase implements RecurrentInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new recurrent entity is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['interval'] = BaseFieldDefinition::create('integer')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Interval'))
      ->setDescription(t('The interval of the Recurrency'))
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'integer',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setSetting('size', 'big');

    $fields['period'] = BaseFieldDefinition::create('list_string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Period'))
      ->setDescription(t('The periods of an Recurrency.'))
      ->setRequired(FALSE)
      ->setSettings([
        'max_length' => 30,
        'text_processing' => 0,
        'allowed_values' => [
          'Day' => t('Day'),
          'Month' => t('Month'),
          'Year' => t('Year'),
        ],
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the recurrent author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the recurrent was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the recurrent was last edited.'));

    return $fields;
  }

  /**
   * Gets the interval value.
   *
   * @return int
   *   the value of the interval
   */
  public function getInterval() {
    return $this->get('interval')->value;
  }

  /**
   * Set the interval value.
   *
   * @param int $interval
   *   Value for the interval.
   *
   * @return \Drupal\mono_entities\Entity\Recurrent
   *   This return a entity Recurrent
   */
  public function setInterval($interval) {
    $this->set('interval', $interval);
    return $this;
  }

  /**
   * Get the period value.
   *
   * @return string
   *   Return a value of the period
   */
  public function getPeriod() {
    return $this->get('period')->value;
  }

  /**
   * Set the period value.
   *
   * @param string $period
   *   Value for the period.
   *
   * @return \Drupal\mono_entities\Entity\Recurrent
   *   This return a entity Recurrent
   */
  public function setPeriod($period) {
    $this->set('period', $period);
    return $this;
  }

}
