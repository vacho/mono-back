<?php

namespace Drupal\mono_entities\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\mono_entities\ExchangeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the exchange entity class.
 *
 * @ContentEntityType(
 *   id = "exchange",
 *   label = @Translation("Exchange"),
 *   label_collection = @Translation("Exchanges"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\mono_entities\ExchangeListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\mono_entities\ExchangeAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\mono_entities\Form\ExchangeForm",
 *       "edit" = "Drupal\mono_entities\Form\ExchangeForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "m_exchange",
 *   data_table = "m_exchange_field_data",
 *   revision_table = "m_exchange_revision",
 *   revision_data_table = "m_exchange_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer exchange",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/exchange/add",
 *     "canonical" = "/exchange/{exchange}",
 *     "edit-form" = "/admin/content/exchange/{exchange}/edit",
 *     "delete-form" = "/admin/content/exchange/{exchange}/delete",
 *     "collection" = "/admin/content/exchange"
 *   },
 *   field_ui_base_route = "entity.exchange.settings"
 * )
 */
class Exchange extends RevisionableContentEntityBase implements ExchangeInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new exchange entity is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['value'] = BaseFieldDefinition::create('decimal')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Value'))
      ->setDescription(t('The value of the entity'))
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'decimal',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['date'] = BaseFieldDefinition::create('timestamp')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Date'))
      ->setDescription(t('The date of the exchange'))
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'adobe',
        'type' => 'timestamp',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['source'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Source'))
      ->setDescription(t('The Currency taken like a source'))
      ->setRequired(FALSE)
      ->setSetting('target_type', 'currency')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['destiny'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Destiny'))
      ->setDescription(t('The Currency taken like a destination'))
      ->setRequired(FALSE)
      ->setSetting('target_type', 'currency')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the exchange author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the exchange was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the exchange was last edited.'));

    return $fields;
  }

  /**
   * Get value.
   *
   * @return float
   *   Return a value.
   */
  public function getValue() {
    return $this->get('value')->value;
  }

  /**
   * Set the value.
   *
   * @param float $value
   *   The value.
   *
   * @return \Drupal\mono_entities\Entity\Exchange
   *   This return a entity Exchange.
   */
  public function setValue($value) {
    $this->set('value', $value);
    return $this;
  }

  /**
   * Get the date value.
   *
   * @return int
   *   Return a value of the date.
   */
  public function getDate() {
    return $this->get('date')->value;
  }

  /**
   * Set the date value.
   *
   * @param int $date
   *   The value of a date.
   *
   * @return \Drupal\mono_entities\Entity\Exchange
   *   This return a entity Exchange.
   */
  public function setDate($date) {
    $this->set('date', $date);
    return $this;
  }

  /**
   * Get the source value.
   *
   * @return int
   *   Return id value of the currency.
   */
  public function getSource() {
    return $this->get('source')->target_id;
  }

  /**
   * Set the source value.
   *
   * @param int $source
   *   The value of id currency.
   *
   * @return \Drupal\mono_entities\Entity\Exchange
   *   This return a entity Exchange.
   */
  public function setsource($source) {
    $this->set('source', $source);
    return $this;
  }

  /**
   * Get the destiny value.
   *
   * @return int
   *   Return id value of the currency.
   */
  public function getDestiny() {
    return $this->get('destiny')->target_id;
  }

  /**
   * Set the destiny value.
   *
   * @param int $destiny
   *   The value of id currency.
   *
   * @return \Drupal\mono_entities\Entity\Exchange
   *   This return a entity Exchange.
   */
  public function setDestiny($destiny) {
    $this->set('destiny', $destiny);
    return $this;
  }

}
