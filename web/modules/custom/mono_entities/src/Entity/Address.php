<?php

namespace Drupal\mono_entities\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\mono_entities\AddressInterface;
use Drupal\user\UserInterface;

/**
 * Defines the address entity class.
 *
 * @ContentEntityType(
 *   id = "address",
 *   label = @Translation("Address"),
 *   label_collection = @Translation("Addresses"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\mono_entities\AddressListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\mono_entities\AddressAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\mono_entities\Form\AddressForm",
 *       "edit" = "Drupal\mono_entities\Form\AddressForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "m_address",
 *   data_table = "m_address_field_data",
 *   revision_table = "m_address_revision",
 *   revision_data_table = "m_address_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer address",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "label" = "full_name",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/address/add",
 *     "canonical" = "/address/{address}",
 *     "edit-form" = "/admin/content/address/{address}/edit",
 *     "delete-form" = "/admin/content/address/{address}/delete",
 *     "collection" = "/admin/content/address"
 *   },
 *   field_ui_base_route = "entity.address.settings"
 * )
 */
class Address extends RevisionableContentEntityBase implements AddressInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new address entity is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['full_name'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Full name'))
      ->setDescription(t('The full name of the address.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 100)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['company'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Company'))
      ->setDescription(t('The company of the address.'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 100)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['address_line1'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Address line1'))
      ->setDescription(t('The address line1 of the address.'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 100)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['address_line2'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Address line2'))
      ->setDescription(t('The address line2 of the address.'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 100)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['address_line3'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Address line3'))
      ->setDescription(t('The address line3 of the address.'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 100)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['postal_code'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Postal code'))
      ->setDescription(t('The postal code of the address.'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 30)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['sorting_code'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Sorting code'))
      ->setDescription(t('The sorting code of the address.'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 30)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['dependent_locality'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Dependent locality'))
      ->setDescription(t('The dependent locality of the address.'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 30)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['locality'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Locality'))
      ->setDescription(t('The locality of the address.'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 30)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['administrative_area'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Administrative area'))
      ->setDescription(t('The administrative area of the address.'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 30)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['gps'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Gps'))
      ->setDescription(t('The gps of the address.'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 50)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the address author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the address was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the address was last edited.'));

    return $fields;
  }

  /**
   * Get the full_name.
   *
   * @return string
   *   Return a value of the full_name.
   */
  public function getFullName() {
    return $this->get('full_name')->value;
  }

  /**
   * Set the full_name.
   *
   * @param string $full_name
   *   The value of full_name.
   *
   * @return \Drupal\mono_entities\Entity\Address
   *   This return a entity Address.
   */
  public function setFullName($full_name) {
    $this->set('full_name', $full_name);
    return $this;
  }

  /**
   * Get the company value.
   *
   * @return string
   *   Return a value of the company.
   */
  public function getCompany() {
    return $this->get('company')->value;
  }

  /**
   * Set the company value.
   *
   * @param string $company
   *   The value of company.
   *
   * @return \Drupal\mono_entities\Entity\Address
   *   This return a entity Address.
   */
  public function setCompany($company) {
    $this->set('company', $company);
    return $this;
  }

  /**
   * Get the address_line1 value.
   *
   * @return string
   *   Return a value of the address_line1
   */
  public function getAddressLine1() {
    return $this->get('address_line1')->value;
  }

  /**
   * Set the address_line1 value.
   *
   * @param string $address_line1
   *   The value of address_line1.
   *
   * @return \Drupal\mono_entities\Entity\Address
   *   This return a entity Address.
   */
  public function setAddressLine1($address_line1) {
    $this->set('address_line1', $address_line1);
    return $this;
  }

  /**
   * Get the address_line2 value.
   *
   * @return string
   *   Return a value of the address_line2
   */
  public function getAddressLine2() {
    return $this->get('address_line2')->value;
  }

  /**
   * Set the address_line2 value.
   *
   * @param string $address_line2
   *   The value of the address_line2.
   *
   * @return \Drupal\mono_entities\Entity\Address
   *   This return a entity Address.
   */
  public function setAddressLine2($address_line2) {
    $this->set('address_line2', $address_line2);
    return $this;
  }

  /**
   * Get the address_line3 value.
   *
   * @return string
   *   Return a value of the address_line3.
   */
  public function getAddressLine3() {
    return $this->get('address_line3')->value;
  }

  /**
   * Set the address_line3 value.
   *
   * @param string $address_line3
   *   The value of address_line3.
   *
   * @return \Drupal\mono_entities\Entity\Address
   *   This return a entity Address
   */
  public function setAddressLine3($address_line3) {
    $this->set('address_line3', $address_line3);
    return $this;
  }

  /**
   * Get the postal_code value.
   *
   * @return string
   *   Return a value of the postal_code.
   */
  public function getPostalCode() {
    return $this->get('postal_code')->value;
  }

  /**
   * Set the postal_code value.
   *
   * @param string $postal_code
   *   The value of postal_code.
   *
   * @return \Drupal\mono_entities\Entity\Address
   *   This return a entity Address.
   */
  public function setPostalCode($postal_code) {
    $this->set('postal_code', $postal_code);
    return $this;
  }

  /**
   * Get the sorting_code value.
   *
   * @return string
   *   Return a value of the sorting_code.
   */
  public function getSortingCode() {
    return $this->get('sorting_code')->value;
  }

  /**
   * Set the sorting_code value.
   *
   * @param string $sorting_code
   *   The value of sorting_code.
   *
   * @return \Drupal\mono_entities\Entity\Address
   *   This return a entity Address
   */
  public function setSortingCode($sorting_code) {
    $this->set('sorting_code', $sorting_code);
    return $this;
  }

  /**
   * Get the dependent_locality value.
   *
   * @return string
   *   Return a value of the dependent_locality.
   */
  public function getDependentLocality() {
    return $this->get('dependent_locality')->value;
  }

  /**
   * Set the dependent_locality value.
   *
   * @param string $dependent_locality
   *   The value of dependent_locality.
   *
   * @return \Drupal\mono_entities\Entity\Address
   *   This return a entity Address.
   */
  public function setDependentLocality($dependent_locality) {
    $this->set('dependent_locality', $dependent_locality);
    return $this;
  }

  /**
   * Get the locality value.
   *
   * @return string
   *   Return a value of the locality.
   */
  public function getLocality() {
    return $this->get('locality')->value;
  }

  /**
   * Set the locality value.
   *
   * @param string $locality
   *   The value of locality.
   *
   * @return \Drupal\mono_entities\Entity\Address
   *   This return a entity Address.
   */
  public function setLocality($locality) {
    $this->set('locality', $locality);
    return $this;
  }

  /**
   * Get the administrative_area value.
   *
   * @return string
   *   Return a value of the administrative_area.
   */
  public function getAdministrativeArea() {
    return $this->get('administrative_area')->value;
  }

  /**
   * Set the administrative_area value.
   *
   * @param string $administrative_area
   *   The value of administrative_area.
   *
   * @return \Drupal\mono_entities\Entity\Address
   *   This return a entity Address.
   */
  public function setAdministrativeArea($administrative_area) {
    $this->set('administrative_area', $administrative_area);
    return $this;
  }

  /**
   * Get the gps value.
   *
   * @return string
   *   Return a value of the gps.
   */
  public function getGps() {
    return $this->get('gps')->value;
  }

  /**
   * Set the gps value.
   *
   * @param string $gps
   *   The value of a gps.
   *
   * @return \Drupal\mono_entities\Entity\Address
   *   This return a entity Address.
   */
  public function setGps($gps) {
    $this->set('gps', $gps);
    return $this;
  }

}
