<?php

namespace Drupal\mono_entities\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\mono_entities\PlaceInterface;
use Drupal\user\UserInterface;

/**
 * Defines the place entity class.
 *
 * @ContentEntityType(
 *   id = "place",
 *   label = @Translation("Place"),
 *   label_collection = @Translation("Places"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\mono_entities\PlaceListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\mono_entities\PlaceAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\mono_entities\Form\PlaceForm",
 *       "edit" = "Drupal\mono_entities\Form\PlaceForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "m_place",
 *   data_table = "m_place_field_data",
 *   revision_table = "m_place_revision",
 *   revision_data_table = "m_place_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer place",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/place/add",
 *     "canonical" = "/place/{place}",
 *     "edit-form" = "/admin/content/place/{place}/edit",
 *     "delete-form" = "/admin/content/place/{place}/delete",
 *     "collection" = "/admin/content/place"
 *   },
 *   field_ui_base_route = "entity.place.settings"
 * )
 */
class Place extends RevisionableContentEntityBase implements PlaceInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new place entity is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the place.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 100)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['gps'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Gps'))
      ->setDescription(t('The gps of the address.'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 50)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['address'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Address'))
      ->setDescription(t('The Address associated whit this place'))
      ->setRequired(FALSE)
      ->setSetting('target_type', 'address')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the place author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the place was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the place was last edited.'));

    return $fields;
  }

  /**
   * Get the name.
   *
   * @return string
   *   Return a name.
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * Set name.
   *
   * @param string $name
   *   The value of a name.
   *
   * @return \Drupal\mono_entities\Entity\Place
   *   This return a entity Place.
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * Get the gps value.
   *
   * @return string
   *   Return a value of the gps.
   */
  public function getGps() {
    return $this->get('gps')->value;
  }

  /**
   * Set the gps value.
   *
   * @param string $gps
   *   The value of a gps.
   *
   * @return \Drupal\mono_entities\Entity\Place
   *   This return a entity Place.
   */
  public function setGps($gps) {
    $this->set('gps', $gps);
    return $this;
  }

  /**
   * Get the address value.
   *
   * @return int
   *   Return id value of the address.
   */
  public function getAddress() {
    return $this->get('address')->target_id;
  }

  /**
   * Set the address value.
   *
   * @param int $address
   *   The value of id address.
   *
   * @return \Drupal\mono_entities\Entity\Place
   *   This return a entity Place.
   */
  public function setAddress($address) {
    $this->set('address', $address);
    return $this;
  }

}
