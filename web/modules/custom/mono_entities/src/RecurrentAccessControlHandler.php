<?php

namespace Drupal\mono_entities;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the recurrent entity type.
 */
class RecurrentAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view recurrent');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit recurrent', 'administer recurrent'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete recurrent', 'administer recurrent'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create recurrent', 'administer recurrent'], 'OR');
  }

}
