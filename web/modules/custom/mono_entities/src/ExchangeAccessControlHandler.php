<?php

namespace Drupal\mono_entities;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the exchange entity type.
 */
class ExchangeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view exchange');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit exchange', 'administer exchange'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete exchange', 'administer exchange'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create exchange', 'administer exchange'], 'OR');
  }

}
