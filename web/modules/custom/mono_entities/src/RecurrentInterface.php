<?php

namespace Drupal\mono_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a recurrent entity type.
 */
interface RecurrentInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the recurrent creation timestamp.
   *
   * @return int
   *   Creation timestamp of the recurrent.
   */
  public function getCreatedTime();

  /**
   * Sets the recurrent creation timestamp.
   *
   * @param int $timestamp
   *   The recurrent creation timestamp.
   *
   * @return \Drupal\mono_entities\RecurrentInterface
   *   The called recurrent entity.
   */
  public function setCreatedTime($timestamp);

}
