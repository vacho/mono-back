/**
 * @file
 * Set Datable operations to table of exchanges.
 */

(function ($) {
  $(document).ready(function () {
    var currency = getUrlParameter('currency');
    $('#td-exchanges').DataTable({
        "columns": [
          { "searchable": false },
          { "searchable": false },
          { "searchable": false },
          { "searchable": false },
          null,
          { "searchable": false },
          { "searchable": false },
          { "searchable": false },
          { "searchable": false },
        ],
      "oSearch": {"sSearch": currency }
    });
  });

}(jQuery));
