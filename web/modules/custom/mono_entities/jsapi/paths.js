/**
 * @file
 * Set of javascript functions to manage paths.
 */

/**
 * Get the parameter passed in url.
 *
 * @param string sPram
 *   The text to identify the param to get the value in the path.
 *
 * @return string
 *   This return the value of the parameter.
 */
var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
};
